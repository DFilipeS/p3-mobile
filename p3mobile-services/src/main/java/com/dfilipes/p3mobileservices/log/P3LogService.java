package com.dfilipes.p3mobileservices.log;

import android.util.Log;

import com.dfilipes.p3mobilenetwork.link.P3NetworkProtos;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobileservices.P3Service;

/**
 * Created by Daniel Filipe on 04/02/16.
 */
public class P3LogService implements P3Service, P3NetworkManager.P3NetworkLogListener {
    private final String LOG_TAG = P3LogService.class.getSimpleName();

    @Override
    public void start() {
        P3NetworkManager.getInstance().addLogListener(this);
    }

    @Override
    public String getServiceName() {
        return "P3Log";
    }

    @Override
    public void incomingMessage(P3NetworkProtos.Message message) {
        return;
    }

    @Override
    public void logMessage(String title, String message) {
            Log.d(LOG_TAG, "[" + title + "] " + message);
    }
}
