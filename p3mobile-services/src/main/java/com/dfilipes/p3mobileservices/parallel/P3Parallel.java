package com.dfilipes.p3mobileservices.parallel;

/**
 * Created by Daniel Filipe on 04/02/16.
 */
public interface P3Parallel extends Runnable {

    void p3main();
    void p3divide();
    void p3restart();
    void p3shutdown();

    void stop();
    boolean isStopped();
    P3Parallel getTask();
}
