package com.dfilipes.p3mobileservices.parallel;

import com.dfilipes.p3mobilenetwork.NetworkGlobals;
import com.dfilipes.p3mobilenetwork.link.P3NetworkProtos;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobilenetwork.network.topology.P3Cell;
import com.dfilipes.p3mobilenetwork.network.topology.P3Node;
import com.dfilipes.p3mobileservices.P3Service;
import com.google.protobuf.ByteString;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

/**
 * Created by Daniel Filipe on 04/02/16.
 */
public class P3ParallelService implements P3Service {
    private final String LOG_TAG = P3ParallelService.class.getSimpleName();
    private P3NetworkManager networkManager;
    private HashMap<P3NetworkProtos.Node, P3Parallel> tasksPool;

    public P3NetworkProtos.Node taskSource;
    public P3Parallel currentTask;
    public Thread executionThread;

    public P3ParallelService() {
        this.networkManager = P3NetworkManager.getInstance();
        this.currentTask = null;
        this.tasksPool = new HashMap<>();
    }

    public void startTask(P3Parallel task) {
        this.currentTask = task;
        executionThread = new Thread(currentTask);
        executionThread.start();
    }

    public void lookForWork() {
        this.currentTask = null;

        if (this.taskSource != null && tasksPool.isEmpty()) {
            networkManager.send(this.taskSource.getAddress(), this.taskSource.getPort(), "P3TASK COMPLETE");
            this.taskSource = null;
        }

        // Check his pool, if pool is empty or every node with task is still alive ask for work from the top again.
        Set<P3NetworkProtos.Node> workingNodes = tasksPool.keySet();
        for (P3NetworkProtos.Node node : workingNodes) {
            // Check if node is still part of the network
            P3Cell cell = null;
            if (networkManager.getCell().getId().equals(node.getId().substring(0, node.getId().length() - 2))) {
                cell = networkManager.getCell();
            } else {
                P3Cell descendantCell = networkManager.getCell().getDescendantCells().get(node.getId().substring(0, node.getId().length() - 2));
                cell = descendantCell;
            }

            if (cell == null || !cell.getMembers().containsKey(node.getId())) {
                // Node is dead, recover this task.
                this.startTask(this.tasksPool.get(node));
                this.tasksPool.remove(node);
                break;
            }
        }

        if (this.currentTask == null && !networkManager.getMyDevice().isRoot()) {
            if (networkManager.getMyDevice().isCoordinator() && networkManager.getCell().getId().equals("0")) {
                networkManager.send(NetworkGlobals.SERVER_IP, NetworkGlobals.SERVER_PORT, "P3DIVIDE");
            } else if (networkManager.getMyDevice().isCoordinator() && !networkManager.getCell().getId().equals("0")) {
                Random generator = new Random();
                Object[] membersArray = networkManager.getCell().getParentCellCoordinators().values().toArray();
                P3Node targetNode = (P3Node) membersArray[generator.nextInt(membersArray.length)];
                networkManager.send(targetNode.getAddress(), targetNode.getPort(), "P3DIVIDE");
            } else {
                Random generator = new Random();
                Object[] membersArray = networkManager.getCell().getCoordinators().values().toArray();
                P3Node targetNode =  (P3Node) membersArray[generator.nextInt(membersArray.length)];
                networkManager.send(targetNode.getAddress(), targetNode.getPort(), "P3DIVIDE");
            }
        }
    }

    @Override
    public void start() {}

    @Override
    public String getServiceName() {
        return "P3Parallel";
    }

    @Override
    public void incomingMessage(P3NetworkProtos.Message message) {
        switch (message.getTitle()) {
            case "P3DIVIDE":
                if (currentTask != null) {
                    currentTask.stop();
                    currentTask.p3divide();

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    try {
                        P3Parallel newTask = currentTask.getTask();
                        ObjectOutput objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                        objectOutputStream.writeObject(newTask);
                        ByteString byteString = ByteString.copyFrom(byteArrayOutputStream.toByteArray());

                        P3NetworkManager.getInstance().send(message.getSender().getAddress(), message.getSender().getPort(), "P3DIVIDE OK", byteString);
                        this.tasksPool.put(message.getSender(), newTask);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    currentTask.p3restart();
                } else {
                    P3NetworkManager.getInstance().send(message.getSender().getAddress(), message.getSender().getPort(), "P3DIVIDE ERROR");
                }
                break;
            case "P3DIVIDE OK":
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(message.getPayload(0).toByteArray());
                ObjectInput objectInput = null;
                try {
                    objectInput = new ObjectInputStream(byteArrayInputStream);

                    P3Parallel task = (P3Parallel) objectInput.readObject();
                    this.startTask(task);
                    this.taskSource = message.getSender();

                    networkManager.log("Task received from " + message.getSender().getId());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                break;
            case "P3DIVIDE ERROR":
                try {
                    Thread.currentThread().sleep(5000);
                    this.lookForWork();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case "P3TASK COMPLETE":
                this.tasksPool.remove(message.getSender());
                break;
            default:
                break;
        }
    }
}
