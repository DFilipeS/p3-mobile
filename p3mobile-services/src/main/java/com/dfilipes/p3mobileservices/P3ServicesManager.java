package com.dfilipes.p3mobileservices;

import com.dfilipes.p3mobilenetwork.link.P3NetworkProtos;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobileservices.filesystem.P3FileSystemService;
import com.dfilipes.p3mobileservices.log.P3LogService;
import com.dfilipes.p3mobileservices.parallel.P3ParallelService;

import java.util.HashMap;

/**
 * Created by Daniel Filipe on 30/01/16.
 */
public class P3ServicesManager implements P3NetworkManager.P3NetworkListener {
    private final String LOG_TAG = P3ServicesManager.class.getSimpleName();

    public static P3ServicesManager instance = null;
    private HashMap<String, P3Service> services = null;

    public P3ServicesManager() {
        instance = this;
        P3NetworkManager.getInstance().addListener(this);

        P3LogService logService = new P3LogService();
        P3FileSystemService fileSystemService = new P3FileSystemService();
        P3ParallelService parallelService = new P3ParallelService();

        services = new HashMap<>();
        services.put(logService.getServiceName(), logService);
        services.put(fileSystemService.getServiceName(), fileSystemService);
        //services.put(parallelService.getServiceName(), parallelService);

        if (P3NetworkManager.getInstance().getMyDevice().isRoot()) {
            this.startServices();
        }
    }

    public static P3ServicesManager getInstance() {
        if (instance == null) {
            instance = new P3ServicesManager();
        }

        return instance;
    }

    private void startServices() {
        for (P3Service service : this.services.values()) {
            service.start();
        }
    }

    public P3Service getService(String serviceName) {
        return services.get(serviceName);
    }

    public void relayMessage(P3NetworkProtos.Message message) {
        // Only start services when network is up
        switch (message.getTitle()) {
            case "JOIN OK":
            case "CREATE CELL":
                this.startServices();
                break;
        }

        for (P3Service service : this.services.values()) {
            service.incomingMessage(message);
        }
    }
}
