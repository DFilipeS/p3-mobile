package com.dfilipes.p3mobileservices.filesystem;

import com.google.protobuf.ByteString;

import java.util.ArrayList;

/**
 * Created by Daniel Filipe on 18/03/16.
 */
public class P3FSMetadata {
    ArrayList<String> locations;
    ArrayList<String> descendants;
    boolean isReplica;
    String type;
    int size;

    public ByteString toByteString() {
        P3FileSystemProtos.Metadata.Builder builder = P3FileSystemProtos.Metadata.newBuilder();
        builder.addAllDescendants(descendants);
        builder.addAllLocations(locations);
        builder.setIsReplica(isReplica);
        builder.setType(type);
        builder.setSize(size);

        return builder.build().toByteString();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (locations != null) builder.append(locations.toString() + ", ");
        else builder.append("null, ");
        if (descendants != null) builder.append(descendants.toString() + ", ");
        else builder.append("null, ");
        builder.append(isReplica + ", ");
        builder.append(type + ", ");
        builder.append(size);

        return builder.toString();
    }
}
