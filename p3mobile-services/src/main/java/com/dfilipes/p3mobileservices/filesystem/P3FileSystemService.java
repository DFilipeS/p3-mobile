package com.dfilipes.p3mobileservices.filesystem;

import android.content.Context;

import com.dfilipes.p3mobilenetwork.P3App;
import com.dfilipes.p3mobilenetwork.link.P3NetworkProtos;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobilenetwork.network.topology.P3Node;
import com.dfilipes.p3mobileservices.P3Service;
import com.google.protobuf.InvalidProtocolBufferException;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Daniel Filipe on 01/02/16.
 */
public class P3FileSystemService implements P3Service {
    public ConcurrentHashMap<String, P3FileSystemProtos.Metadata> metadata;

    private P3NetworkManager networkManager;

    @Override
    public void start() {
        this.metadata = new ConcurrentHashMap<>();
        this.networkManager = P3NetworkManager.getInstance();


        P3Node myDevice = networkManager.getMyDevice();
        if (myDevice.isRoot()) {
            // Create filesystem root ("/").
            P3FileSystemProtos.Metadata.Builder root = P3FileSystemProtos.Metadata.newBuilder();
            root.setIsReplica(false);
            root.setType("dir");
            root.setSize(0);

            this.metadata.put("/", root.build());
        }
    }

    @Override
    public String getServiceName() {
        return "P3FileSystem";
    }

    public void addFile(String key, P3FileSystemProtos.Metadata metadata) {
        this.metadata.put(key, metadata);
        // TODO : Update coordinators and parents
    }

    @Override
    public void incomingMessage(P3NetworkProtos.Message message) {
        switch (message.getTitle()) {
            case "JOIN OK":
                if (networkManager.getMyDevice().isCoordinator()) {
                    networkManager.send(message.getSender().getAddress(), message.getSender().getPort(), "P3FS COORDINATOR UPDATE");
                }
                break;
            case "P3FS UPDATE":
                // Update current
                try {
                    P3FileSystemProtos.MetadataMap updatedMetadata = P3FileSystemProtos.MetadataMap.parseFrom(message.getPayload(0));
                    this.metadata.putAll(updatedMetadata.getMetadataMap());
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }

                // Update other coordinators
                for (Map.Entry<String, P3Node> entry : networkManager.getCell().getCoordinators().entrySet()) {
                    if (networkManager.getMyDevice().getId() != entry.getKey()) {
                        P3FileSystemProtos.MetadataMap.Builder builder = P3FileSystemProtos.MetadataMap.newBuilder();
                        builder.putAllMetadataMap(metadata);
                        networkManager.send(entry.getValue().getAddress(), entry.getValue().getPort(), "P3FS COORDINATOR UPDATE OK", builder.build().toByteString());
                    }
                }

                // Propagate information to a random coordinator on parent cell
                if ( ! networkManager.getCell().getParentCellCoordinators().isEmpty()) {
                    for (Map.Entry<String, P3Node> entry : networkManager.getCell().getParentCellCoordinators().entrySet()) {
                        P3FileSystemProtos.MetadataMap.Builder builder = P3FileSystemProtos.MetadataMap.newBuilder();
                        builder.putAllMetadataMap(metadata);
                        networkManager.send(entry.getValue().getAddress(), entry.getValue().getPort(), "P3FS UPDATE", builder.build().toByteString());
                        break;
                    }
                }
                break;
            case "P3FS COORDINATOR UPDATE":
                P3FileSystemProtos.MetadataMap.Builder builder = P3FileSystemProtos.MetadataMap.newBuilder();
                builder.putAllMetadataMap(metadata);
                networkManager.send(message.getSender().getAddress(), message.getSender().getPort(), "P3FS COORDINATOR UPDATE OK", builder.build().toByteString());
                break;
            case "P3FS COORDINATOR UPDATE OK":
                try {
                    P3FileSystemProtos.MetadataMap metadataMap = P3FileSystemProtos.MetadataMap.parseFrom(message.getPayload(0));
                    this.metadata.putAll(metadataMap.getMetadataMap());
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                break;
            case "P3FS GET FILE":
                String filePath = new String(message.getPayload(0).toByteArray());
                if (this.metadata.containsKey(filePath)) {
                    // TODO : Send actual file
                    File file = new File(P3App.getContext().getFilesDir() + "/" + filePath);
                    if (file.exists()) {
                        networkManager.sendFile(message.getSender().getAddress(), message.getSender().getFilesPort(), filePath);
                    }
                } else {
                    if (networkManager.getCell().getId() == "0") {
                        networkManager.send(message.getSender().getAddress(), message.getSender().getPort(), "P3FS FILE NOT FOUND");
                    } else {
                        P3Node nextNode = networkManager.getCell().getParentCellCoordinators().values().iterator().next();
                        networkManager.send(message.getSender().getAddress(), message.getSender().getPort(), "P3FS FILE REDIRECT", message.getPayload(0), nextNode.getByteString());
                    }
                }
                break;
            case "P3FS FILE REDIRECT":
                try {
                    P3NetworkProtos.Node nextNode = P3NetworkProtos.Node.parseFrom(message.getPayload(1));
                    networkManager.send(nextNode.getAddress(), nextNode.getPort(), "P3FS GET FILE", message.getPayload(0));
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                break;
            case "P3FS FILE NOT FOUND":
                // TODO : Handle file not found
                break;
            default:
                P3NetworkManager.getInstance().log("[P3FileSystemService] Received " + message.getTitle() + " message.");
                break;
        }
    }
}
