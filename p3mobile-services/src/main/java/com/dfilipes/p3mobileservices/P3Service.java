package com.dfilipes.p3mobileservices;

import com.dfilipes.p3mobilenetwork.link.P3NetworkProtos;

/**
 * Created by Daniel Filipe on 30/01/16.
 */
public interface P3Service {

    void start();
    String getServiceName();
    void incomingMessage(P3NetworkProtos.Message message);
}
