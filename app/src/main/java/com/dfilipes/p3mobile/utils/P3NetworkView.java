package com.dfilipes.p3mobile.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.dfilipes.p3mobilenetwork.network.topology.P3Cell;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Daniel Filipe on 20/11/15.
 */
public class P3NetworkView extends View {
    private Paint mCellPaint;
    private Paint mCellTextPaint;
    private Paint mLogoTextPaint;
    private Paint mLogoPaint;
    private int middlePoint = 0;
    private int canvasWidth = 0;

    private P3Cell network;

    private int cellWidth = 30;
    private int nCircles = 4;

    private final String LOG_TAG = P3NetworkView.class.getSimpleName();

    public P3NetworkView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mCellPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCellPaint.setColor(Color.BLUE);
        mCellPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mCellPaint.setStrokeWidth(5);

        mCellTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCellTextPaint.setColor(Color.WHITE);
        mCellTextPaint.setStyle(Paint.Style.STROKE);
        mCellTextPaint.setStrokeWidth(5);
        mCellTextPaint.setTextSize(50);
        mCellTextPaint.setTextAlign(Paint.Align.CENTER);

        mLogoPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLogoPaint.setColor(Color.BLUE);
        mLogoPaint.setStyle(Paint.Style.FILL);
        mLogoPaint.setStrokeWidth(5);

        mLogoTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLogoTextPaint.setColor(Color.WHITE);
        mLogoTextPaint.setStyle(Paint.Style.FILL);
        mLogoTextPaint.setTextSize(100);
        mLogoTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        middlePoint = MeasureSpec.getSize(widthMeasureSpec) / 2;
        canvasWidth = MeasureSpec.getSize(widthMeasureSpec);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        /* Logo */
        canvas.drawCircle(middlePoint, 105, 100, mLogoPaint);
        canvas.drawRect(middlePoint - 100, 5, middlePoint, 300, mLogoPaint);
        canvas.drawText("3", middlePoint, 140, mLogoTextPaint);

        /**
         *  Network
         */
        drawTree(network, Math.round(canvasWidth * (float) 0.5), 500, -1, -1, canvas);
    }

    private void drawTree(P3Cell cell, int hPosition, int vPosition, int parentHPosition, int parentVPosition, Canvas canvas) {
        if (cell.hasDescendantCells()) {
            Map<String, P3Cell> descendants = cell.getDescendantCells();
            int counter = 1;
            for (Map.Entry<String, P3Cell> entry : descendants.entrySet()) {
                P3Cell descendantCell = entry.getValue();

                //float hPositionDivision = (float) (2 * hPosition) * ((float) counter / (float) (descendants.size() + 1));
                float hPositionDivision = (float) (2 * hPosition) * ((float) counter / (float) (descendants.size() + 1));

                drawTree(descendantCell, Math.round(hPositionDivision), vPosition + 200, hPosition, vPosition, canvas);
                counter++;
            }
        }

        if (parentHPosition != -1)
            canvas.drawLine(hPosition, vPosition, parentHPosition, parentVPosition, mCellPaint);
        canvas.drawCircle(hPosition, vPosition, cellWidth, mCellPaint);
        canvas.drawText(cell.getMembers().size() + "", hPosition, vPosition + 15, mCellTextPaint);
    }

    public void setNetwork(P3Cell network) {
        this.network = network;
    }

    public void redraw() {
        this.invalidate();
    }
}
