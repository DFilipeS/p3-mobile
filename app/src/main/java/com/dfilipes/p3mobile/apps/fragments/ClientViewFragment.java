package com.dfilipes.p3mobile.apps.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dfilipes.p3mobile.R;
import com.dfilipes.p3mobilenetwork.NetworkGlobals;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobileservices.P3ServicesManager;
import com.dfilipes.p3mobileservices.parallel.P3ParallelService;

public class ClientViewFragment extends Fragment implements P3NetworkManager.P3NetworkLogListener {
    private final String LOG_TAG = ClientViewFragment.class.getSimpleName();

    private P3NetworkManager networkManager;
    private P3ServicesManager servicesManager;
    private View view;

    public ClientViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        networkManager = new P3NetworkManager();
        networkManager.join();

        servicesManager = new P3ServicesManager();
        networkManager.addLogListener(this);

        while (!networkManager.isNetworkReady()) {} // Active wait for network to get ready.
        ((P3ParallelService) servicesManager.getService("P3Parallel")).lookForWork();

        //networkManager.send(NetworkGlobals.SERVER_IP, NetworkGlobals.SERVER_PORT, "P3DIVIDE");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_client_view, container, false);

        return view;
    }

    public void printToLogView(String message) {
        final TextView logTextView = (TextView) view.findViewById(R.id.log_textview);
        logTextView.setText(message + '\n' + logTextView.getText());
    }

    public void printToStatsView(String stats) {
        final TextView statsTextView = (TextView) view.findViewById(R.id.statistics_textview);
        statsTextView.setText(stats);
    }

    @Override
    public void logMessage(final String title, final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (title) {
                    case "STATS":
                        float ratio = ((float) networkManager.protoBufByteCounter / networkManager.serializationByteCounter) * 100;
                        String protobuf = networkManager.protoBufByteCounter + " bytes with Protocol Buffers\n";
                        String serialization = ratio + "% of the original space\n" + networkManager.serializationByteCounter + " bytes with serialization\n";
                        String stats = protobuf + serialization;
                        ClientViewFragment.this.printToStatsView(stats);
                        break;
                    default:
                        ClientViewFragment.this.printToLogView(message);
                        break;
                }
            }
        });
    }
}
