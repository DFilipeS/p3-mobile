package com.dfilipes.p3mobile.apps;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.dfilipes.p3mobile.R;
import com.dfilipes.p3mobile.utils.P3NetworkView;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobilenetwork.network.topology.P3Cell;

public class NetworkViewActivity extends AppCompatActivity implements P3NetworkManager.P3NetworkLogListener {
    private final String LOG_TAG = NetworkViewActivity.class.getSimpleName();
    private P3NetworkView networkView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_view);

        P3Cell network = P3NetworkManager.getInstance().getCell();
        networkView = (P3NetworkView) findViewById(R.id.network_view);
        networkView.setNetwork(network);

        Log.d(LOG_TAG, network.getDescendantCells().toString());

        P3NetworkManager.getInstance().addLogListener(this);
    }

    @Override
    public void logMessage(String title, String message) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                networkView.redraw();
            }
        });
    }
}
