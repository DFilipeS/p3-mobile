package com.dfilipes.p3mobile.apps.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dfilipes.p3mobile.R;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobileservices.P3ServicesManager;

public class MainActivityFragment extends Fragment {
    private final String LOG_TAG = MainActivityFragment.class.getSimpleName();

    private View view;

    public MainActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);

        this.initButtonListeners(view);

        return view;
    }

    private void initButtonListeners(final View view) {
        final Button startComputationButton = (Button) view.findViewById(R.id.start_computation_button);
        final Button joinComputationButton = (Button) view.findViewById(R.id.join_computation_button);

        startComputationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServerViewFragment serverViewFragment = new ServerViewFragment();
                final FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment, serverViewFragment);
                fragmentTransaction.commit();
            }
        });

        joinComputationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClientViewFragment clientViewFragment = new ClientViewFragment();
                final FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment, clientViewFragment);
                fragmentTransaction.commit();
            }
        });
    }
}
