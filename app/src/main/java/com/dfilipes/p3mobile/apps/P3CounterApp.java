package com.dfilipes.p3mobile.apps;

import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobileservices.P3ServicesManager;
import com.dfilipes.p3mobileservices.filesystem.P3FileSystemService;
import com.dfilipes.p3mobileservices.parallel.P3Parallel;
import com.dfilipes.p3mobileservices.parallel.P3ParallelService;

import java.io.Serializable;

/**
 * Created by Daniel Filipe on 04/02/16.
 */
public class P3CounterApp implements P3Parallel, Serializable {
    private static final long serialVersionUID = 42L;
    public P3CounterApp newTask;

    private boolean stopThread = false;

    private final String LOG_TAG = P3CounterApp.class.getSimpleName();

    public int start;
    public int end;

    public P3CounterApp(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public void stop() {
        this.stopThread = true;
    }

    public boolean isStopped() {
        return this.stopThread;
    }

    @Override
    public void p3main() {}

    @Override
    public void p3divide() {
        int otherStart = (end + start) / 2;
        int otherEnd = end;
        this.end = otherStart - 1;

        newTask = new P3CounterApp(otherStart, otherEnd);
    }

    @Override
    public void p3restart() {
        P3NetworkManager.getInstance().log("From " + start + " to " + end);
        this.stopThread = false;
    }

    @Override
    public void p3shutdown() {
        ((P3ParallelService) (P3ServicesManager.getInstance().getService("P3Parallel"))).lookForWork();
    }

    @Override
    public P3Parallel getTask() {
        return this.newTask;
    }

    @Override
    public void run() {
        P3NetworkManager.getInstance().log("From " + start + " to " + end);
        while (start <= end) {
            try {
                P3NetworkManager.getInstance().log(LOG_TAG + " - [" + Thread.currentThread().getId() + "] " + start + "");
                start++;
                Thread.currentThread().sleep(1000);

                while (this.stopThread) {}
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.p3shutdown();
    }
}
