package com.dfilipes.p3mobile.apps.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.dfilipes.p3mobile.R;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobilenetwork.network.topology.P3Node;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class NetworkListActivityFragment extends Fragment implements P3NetworkManager.P3NetworkLogListener {
    private final String LOG_TAG = NetworkListActivityFragment.class.getSimpleName();
    private NetworkListAdapter adapter = null;

    public NetworkListActivityFragment() {
        P3NetworkManager.getInstance().addLogListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_network_list, container, false);
        final ListView listView = (ListView) view.findViewById(R.id.networkListView);
        final P3NetworkManager networkManager = P3NetworkManager.getInstance();

        adapter = new NetworkListAdapter(getContext());

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                P3Node node = (P3Node) listView.getAdapter().getItem(position);
                networkManager.send(node.getAddress(), node.getPort(), "HELLO");
            }
        });
        return view;
    }

    @Override
    public void logMessage(String title, String message) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    NetworkListActivityFragment.this.adapter.notifyDataSetChanged();
                }
            });
        }
    }

    class NetworkListAdapter extends BaseAdapter {
        private List<P3Node> nodeList;
        private Context context;
        private P3NetworkManager networkManager;

        public NetworkListAdapter(Context context) {
            this.context = context;
            networkManager = P3NetworkManager.getInstance();
            nodeList = P3NetworkManager.getInstance().listNetwork(networkManager.getCell());
        }

        @Override
        public int getCount() {
            return P3NetworkManager.getInstance().listNetwork(networkManager.getCell()).size();
        }

        @Override
        public Object getItem(int position) {
            return P3NetworkManager.getInstance().listNetwork(networkManager.getCell()).get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.network_list_item, parent, false);

            final TextView text = (TextView) view.findViewById(R.id.network_list_node_id);
            P3Node node = (P3Node) this.getItem(position);
            text.setText("[" + node.getId() + "] " + node.getAddress() + ":" + node.getPort());

            return view;
        }
    }
}
