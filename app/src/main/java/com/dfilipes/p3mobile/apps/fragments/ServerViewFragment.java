package com.dfilipes.p3mobile.apps.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dfilipes.p3mobile.R;
import com.dfilipes.p3mobile.apps.P3CounterApp;
import com.dfilipes.p3mobilenetwork.NetworkGlobals;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobilenetwork.network.P3NetworkUtils;
import com.dfilipes.p3mobileservices.P3ServicesManager;
import com.dfilipes.p3mobileservices.parallel.P3ParallelService;

public class ServerViewFragment extends Fragment implements P3NetworkManager.P3NetworkLogListener {
    private final String LOG_TAG = ServerViewFragment.class.getSimpleName();

    private P3NetworkManager networkManager;
    private P3ServicesManager servicesManager;
    private View view;

    public ServerViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        networkManager = new P3NetworkManager(NetworkGlobals.SERVER_IP, NetworkGlobals.SERVER_PORT);
        servicesManager = new P3ServicesManager();

        networkManager.addLogListener(this);
        P3ParallelService parallelService = (P3ParallelService) servicesManager.getService("P3Parallel");
        if (parallelService != null) parallelService.startTask(new P3CounterApp(0, 100));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_server_view, container, false);

        final TextView ipAddressTextView = (TextView) view.findViewById(R.id.start_computation_ip_address);
        ipAddressTextView.setText(P3NetworkUtils.getIpAddress(getContext()));

        return view;
    }

    public void printToLogView(String message) {
        final TextView logTextView = (TextView) view.findViewById(R.id.log_textview);
        logTextView.setText(message + '\n' + logTextView.getText());
    }

    public void printToStatsView(String stats) {
        final TextView statsTextView = (TextView) view.findViewById(R.id.statistics_textview);
        statsTextView.setText(stats);
    }

    @Override
    public void logMessage(final String title, final String message) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switch (title) {
                        case "STATS":
                            float ratio =  ((float) networkManager.protoBufByteCounter / networkManager.serializationByteCounter) * 100;
                            String protobuf = networkManager.protoBufByteCounter + " bytes with Protocol Buffers\n";
                            String serialization = ratio + "% of the original space\n" + networkManager.serializationByteCounter + " bytes with serialization\n";
                            String stats = protobuf + serialization;
                            ServerViewFragment.this.printToStatsView(stats);
                            break;
                        default:
                            ServerViewFragment.this.printToLogView(message);
                            break;
                    }
                }
            });
        }
    }
}
