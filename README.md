# P3 Mobile #

**P3-Mobile** is a framework for developing parallel peer-to-peer distributed applications on mobile devices running Android OS. This new framework provides developers a quick and easy tool for develop- ment of distributed applications on edge clouds.

It abstracts network formation and maintenance to the framework so the developers only need to define what their applications need to do and how work must be divided upon request by other peers. This allows to easily crowd source the processing power of several mobile devices that are idle (most of the time, mobile devices use only a fraction of their processing power) to complete some high load task.