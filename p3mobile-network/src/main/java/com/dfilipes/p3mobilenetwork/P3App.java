package com.dfilipes.p3mobilenetwork;

import android.app.Application;
import android.content.Context;

/**
 * Created by danielfilipe on 31/03/16.
 */
public class P3App extends Application {
    private static P3App instance;

    public static P3App getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }
}
