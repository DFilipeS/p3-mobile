package com.dfilipes.p3mobilenetwork.network;

import com.dfilipes.p3mobilenetwork.NetworkGlobals;
import com.dfilipes.p3mobilenetwork.link.P3FileReceiver;
import com.dfilipes.p3mobilenetwork.link.P3FileSender;
import com.dfilipes.p3mobilenetwork.link.P3NetworkProtos;
import com.dfilipes.p3mobilenetwork.link.P3Receiver;
import com.dfilipes.p3mobilenetwork.link.P3Sender;
import com.dfilipes.p3mobilenetwork.network.topology.P3Cell;
import com.dfilipes.p3mobilenetwork.network.topology.P3Node;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Daniel Filipe on 11/11/15.
 */
public class P3NetworkManager implements Serializable {
    private static final long serialVersionUID = 42L;
    private List<P3NetworkListener> listeners = new ArrayList<>();
    private List<P3NetworkLogListener> logListeners = new ArrayList<>();
    private int nextCellId = -1;
    private P3Node myDevice;
    private P3Cell cell;
    private boolean networkReady = false;
    private final String LOG_TAG = P3NetworkManager.class.getSimpleName();

    public static P3NetworkManager instance = null;
    public int protoBufByteCounter = 0;
    public int serializationByteCounter = 0;

    /**
     * General constructor for every node, except the root.
     */
    public P3NetworkManager() {
        instance = this;

        myDevice = new P3Node();
        myDevice.setAddress(NetworkGlobals.INTERFACE_IP);
        new Thread(new P3Receiver()).start();
        new Thread(new P3FileReceiver()).start();
    }

    /**
     * Constructor dedicated to the initial node in the network.
     * @param address The server IP address
     * @param port The server port
     */
    public P3NetworkManager(String address, int port) {
        instance = this;

        myDevice = new P3Node();
        myDevice.setId("0:0");
        myDevice.setAddress(address);
        myDevice.setPort(port);

        cell = new P3Cell("0");
        cell.addCoordinator(myDevice);
        cell.addMember(myDevice);

        // Start listening for incoming connections
        new Thread(new P3Receiver(NetworkGlobals.SERVER_PORT)).start();
        new Thread(new P3FileReceiver()).start();

        // Make root P3Node know he is coordinator.
        myDevice.startCoordinationRoutines();
    }

    public static P3NetworkManager getInstance() {
        if (instance == null) {
            instance = new P3NetworkManager();
        }

        return instance;
    }

    /**
     * Sends a message to the device with IP address and port specified.
     * @param address IP address of the destination device.
     * @param port Port number of the destination device.
     * @param title Message to send.
     * @param payload
     */
    public void send(String address, int port, String title, ByteString... payload) {
        try {
            new Thread(new P3Sender(InetAddress.getByName(address), port, title, payload)).start();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void sendFile(String address, int port, String filename) {
        try {
            new Thread(new P3FileSender(InetAddress.getByName(address), port, filename)).start();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * Request message to join the P3 network.
     */
    public void join() {
        myDevice = new P3Node();
        myDevice.setAddress(NetworkGlobals.INTERFACE_IP);

        while (myDevice.getPort() == 0) {} // Wait until ServerSocket is up

        // Ask root to join
        this.send(NetworkGlobals.SERVER_IP, NetworkGlobals.SERVER_PORT, "JOIN");
    }

    /**
     * Process incoming messages and do something with it.
     * @param message Message inside the incoming transmission.
     */
    public void processResponse(P3NetworkProtos.Message message) {
        switch (message.getTitle()) {
            case "JOIN":
                new P3JoinStateMachine(cell, new P3Node(message.getSender()));
                break;
            case "JOIN OK":
                try {
                    P3NetworkProtos.Cell cellProtobuf = P3NetworkProtos.Cell.parseFrom(message.getPayload(0));
                    cell = new P3Cell(cellProtobuf);
                    myDevice.setId(new String(message.getPayload(1).toByteArray()));

                    // Make P3Node know if he is coordinator or not.
                    if (myDevice.isCoordinator()) {
                        myDevice.startCoordinationRoutines();
                    }

                    this.networkReady = true;
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                break;
            case "JOIN REDIRECT":
                P3NetworkProtos.Node coordinator = null;

                try {
                    coordinator = P3NetworkProtos.Node.parseFrom(message.getPayload(0));
                    this.send(coordinator.getAddress(), coordinator.getPort(), "JOIN");
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }

                break;
            case "CREATE CELL":
                try {
                    String cellId = new String(message.getPayload(0).toByteArray());
                    P3NetworkProtos.Cell parentCellProtobuf = P3NetworkProtos.Cell.parseFrom(message.getPayload(1));

                    myDevice.setId(cellId + ":0");

                    cell = new P3Cell(cellId);
                    cell.addCoordinator(myDevice);
                    cell.addMember(myDevice);
                    cell.setParentCellCoordinators(parentCellProtobuf.getCoordinators());

                    // Make P3Node know he is coordinator.
                    myDevice.startCoordinationRoutines();

                    for (Map.Entry<String, P3NetworkProtos.Node> entry : parentCellProtobuf.getCoordinators().entrySet()) {
                        this.send(entry.getValue().getAddress(), entry.getValue().getPort(), "CREATE CELL OK", cell.getByteString());
                    }

                    this.networkReady = true;
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }

                break;
            case "CREATE CELL OK":
                P3NetworkProtos.Cell descendantCell = null;
                try {
                    descendantCell = P3NetworkProtos.Cell.parseFrom(message.getPayload(0));
                    this.cell.addDescendantCell(descendantCell);
                    this.cell.updateParentCoordinators();
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                break;
            case "UPDATE":
                P3NetworkProtos.Cell currentCell = null;
                try {
                    currentCell = P3NetworkProtos.Cell.parseFrom(message.getPayload(0));
                    this.cell = new P3Cell(currentCell);
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                break;
            case "UPDATE CELL":
                P3NetworkProtos.Cell updatedChildCell = null;
                try {
                    updatedChildCell = P3NetworkProtos.Cell.parseFrom(message.getPayload(0));
                    this.cell.getDescendantCells().put(updatedChildCell.getId(), new P3Cell(updatedChildCell));
                    this.cell.updateParentCoordinators();
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                break;
            case "UPDATE PARENT COORDINATORS":
                P3NetworkProtos.Cell updatedParentCell = null;
                try {
                    updatedParentCell = P3NetworkProtos.Cell.parseFrom(message.getPayload(0));
                    this.cell.getParentCellCoordinators().clear();
                    for (Map.Entry<String, P3NetworkProtos.Node> entry : updatedParentCell.getCoordinators().entrySet()) {
                        P3Node node = new P3Node(entry.getValue());
                        this.cell.getParentCellCoordinators().put(node.getId(), node);
                    }
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                break;
            case "ELECTED COORDINATOR":
                try {
                    this.cell = new P3Cell(P3NetworkProtos.Cell.parseFrom(message.getPayload(0)));
                    this.myDevice.startCoordinationRoutines();
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                break;
        }

        this.dispatchMessage(message);
    }

    public void removeDevice(String address, int port) {
        for (Map.Entry<String, P3Node> entry : this.getCell().getMembers().entrySet()) {
            P3Node node = entry.getValue();
            if (node.getAddress().equals(address) && node.getPort() == port) {
                if (node.isCoordinator()) {
                    this.getCell().removeMember(node.getId());
                    this.getCell().electNewCoordinator();
                } else {
                    this.getCell().removeMember(node.getId());
                }

                this.getCell().updateParentCoordinators();
                this.getCell().updateCurrentCell();
                return;
            }
        }

        for (Map.Entry<String, P3Node> entry : this.getCell().getParentCellCoordinators().entrySet()) {
            P3Node node = entry.getValue();
            if (node.getAddress().equals(address) && node.getPort() == port) {
                this.getCell().getParentCellCoordinators().remove(node.getId());
                return;
            }
        }

        for (Map.Entry<String, P3Cell> cellEntry : this.getCell().getDescendantCells().entrySet()) {
            P3Cell cell = cellEntry.getValue();
            for (Map.Entry<String, P3Node> entry : cell.getCoordinators().entrySet()) {
                P3Node node = entry.getValue();
                if (node.getAddress().equals(address) && node.getPort() == port) {
                    if (cell.getCoordinators().size() == 1) {
                        this.getCell().getDescendantCells().remove(cell.getId());
                    } else {
                        this.getCell().getDescendantCells().get(cell.getId()).getCoordinators().remove(node.getId());
                    }

                    return;
                }
            }
        }
    }

    public P3Node getMyDevice() {
        return myDevice;
    }

    public P3Cell getCell() {
        return cell;
    }

    public boolean isNetworkReady() {
        return this.networkReady;
    }

    public int getNextCellId() {
        nextCellId += 1;
        return nextCellId;
    }

    public List<P3Node> listNetwork(P3Cell cell) {
        LinkedList<P3Node> nodesList = new LinkedList<>();
        nodesList.addAll(cell.getMembers().values());

        for (P3Cell descendantCell : cell.getDescendantCells().values()) {
            nodesList.addAll(this.listNetwork(descendantCell));
        }

        return nodesList;
    }

    public void addListener(P3NetworkListener listener) {
        this.listeners.add(listener);
    }

    public void addLogListener(P3NetworkLogListener listener) {
        this.logListeners.add(listener);
    }

    private void dispatchMessage(P3NetworkProtos.Message message) {
        for (P3NetworkListener listener : this.listeners) {
            listener.relayMessage(message);
        }
    }

    public void log(String message) {
        for (P3NetworkLogListener listener : this.logListeners) {
            listener.logMessage(LOG_TAG, message);
        }
    }

    public void log(String title, String message) {
        for (P3NetworkLogListener listener : this.logListeners) {
            listener.logMessage(title, message);
        }
    }

    /**
     * Interfaces
     */

    public interface P3NetworkListener {
        void relayMessage(P3NetworkProtos.Message message);
    }

    public interface P3NetworkLogListener {
        void logMessage(String title, String message);
    }
}
