package com.dfilipes.p3mobilenetwork.network.topology;

import com.dfilipes.p3mobilenetwork.NetworkGlobals;
import com.dfilipes.p3mobilenetwork.link.P3NetworkProtos;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.google.protobuf.ByteString;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Daniel Filipe on 11/11/15.
 */
public class P3Cell implements Serializable {
    private static final long serialVersionUID = 42L;

    private String id;
    private P3NetworkManager networkManager;
    private Map<String, P3Node> coordinators;                // <DeviceID, P3Node>
    private Map<String, P3Node> members;                     // <DeviceID, P3Node>
    private Map<String, P3Cell> descendantCells;             // <CellID, P3Cell>
    private Map<String, P3Node> parentCellCoordinators;      // <DeviceID, P3Node>

    public P3Cell(String id) {
        this.id = id;
        this.networkManager = P3NetworkManager.getInstance();
        this.coordinators = new HashMap<>();
        this.members = new HashMap<>();
        this.descendantCells = new HashMap<>();
        this.parentCellCoordinators = new HashMap<>();
    }

    public P3Cell(P3NetworkProtos.Cell cell) {
        this.id = cell.getId();
        this.networkManager = P3NetworkManager.getInstance();
        this.coordinators = new HashMap<>();
        this.members = new HashMap<>();
        this.descendantCells = new HashMap<>();
        this.parentCellCoordinators = new HashMap<>();

        for (Map.Entry<String, P3NetworkProtos.Node> entry : cell.getCoordinators().entrySet()) {
            this.coordinators.put(entry.getKey(), new P3Node(entry.getValue()));
        }

        for (Map.Entry<String, P3NetworkProtos.Node> entry : cell.getMembers().entrySet()) {
            this.members.put(entry.getKey(), new P3Node(entry.getValue()));
        }

        for (Map.Entry<String, P3NetworkProtos.Cell> entry : cell.getDescendantCells().entrySet()) {
            this.descendantCells.put(entry.getKey(), new P3Cell(entry.getValue()));
        }

        for (Map.Entry<String, P3NetworkProtos.Node> entry : cell.getParentCellCoordinators().entrySet()) {
            this.parentCellCoordinators.put(entry.getKey(), new P3Node(entry.getValue()));
        }
    }

    public boolean isFull() {
        return members.size() == NetworkGlobals.CELL_SIZE;
    }

    public boolean hasDescendantCells() {
        return descendantCells.size() != 0;
    }

    public boolean isDescendantsLevelFull() {
        return descendantCells.size() == NetworkGlobals.MAX_CELLS_PER_LEVEL;
    }

    public void addCoordinator(P3Node node) {
        coordinators.put(node.getId(), node);
    }

    public void addMember(P3Node node) {
        members.put(node.getId(), node);

        // Propagate change to parent cells coordinators
        // this.updateParentCoordinators();
    }

    public void removeMember(String id) {
        this.members.remove(id);
        this.coordinators.remove(id);

        // Propagate change to parent cells coordinator
        //this.updateParentCoordinators();
    }

    public void addDescendantCell(P3Cell cell) {
        this.descendantCells.put(cell.getId(), cell);
    }

    public void addDescendantCell(P3NetworkProtos.Cell cell) {
        this.descendantCells.put(cell.getId(), new P3Cell(cell));
    }

    public String getId() {
        return id;
    }

    public int getNextId() {
        char[] chars = "1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        return Integer.valueOf(sb.toString());
    }

    public Map<String, P3Node> getCoordinators() {
        return coordinators;
    }

    public HashMap<String, P3NetworkProtos.Node> getCoordinatorsProtobuf() {
        HashMap<String, P3NetworkProtos.Node> protobufCoordinatorsMap = new HashMap<>();
        for (Map.Entry<String, P3Node> entry : this.coordinators.entrySet()) {
            protobufCoordinatorsMap.put(entry.getKey(), entry.getValue().getProtobuf());
        }

        return protobufCoordinatorsMap;
    }

    public Map<String, P3Node> getMembers() {
        return members;
    }

    public Map<String, P3NetworkProtos.Node> getMembersProtobuf() {
        HashMap<String, P3NetworkProtos.Node> protobufMembersMap = new HashMap<>();
        for (Map.Entry<String, P3Node> entry : this.members.entrySet()) {
            protobufMembersMap.put(entry.getKey(), entry.getValue().getProtobuf());
        }

        return protobufMembersMap;
    }

    public Map<String, P3Cell> getDescendantCells() {
        return descendantCells;
    }

    public Map<String, P3NetworkProtos.Cell> getDescendantCellsProtobuf() {
        HashMap<String, P3NetworkProtos.Cell> protobufDescendantCells = new HashMap<>();
        for (Map.Entry<String, P3Cell> entry : this.descendantCells.entrySet()) {
            protobufDescendantCells.put(entry.getKey(), entry.getValue().getProtobuf());
        }

        return protobufDescendantCells;
    }

    public Map<String, P3Node> getParentCellCoordinators() {
        return parentCellCoordinators;
    }

    public Map<String, P3NetworkProtos.Node> getParentCellCoordinatorsProtobuf() {
        HashMap<String, P3NetworkProtos.Node> protobufParentCellCoordinatorsMap = new HashMap<>();
        for (Map.Entry<String, P3Node> entry : this.parentCellCoordinators.entrySet()) {
            protobufParentCellCoordinatorsMap.put(entry.getKey(), entry.getValue().getProtobuf());
        }

        return protobufParentCellCoordinatorsMap;
    }

    public void setParentCellCoordinators(HashMap<String, P3Node> parentCellCoordinators) {
        this.parentCellCoordinators = parentCellCoordinators;
    }

    public void setParentCellCoordinators(Map<String, P3NetworkProtos.Node> parentCellCoordinators) {
        for (Map.Entry<String, P3NetworkProtos.Node> entry : parentCellCoordinators.entrySet()) {
            this.parentCellCoordinators.put(entry.getKey(), new P3Node(entry.getValue()));
        }
    }

    public P3NetworkProtos.Cell getProtobuf() {
        P3NetworkProtos.Cell.Builder builder = P3NetworkProtos.Cell.newBuilder();
        builder.setId(this.id);
        builder.putAllCoordinators(this.getCoordinatorsProtobuf());
        builder.putAllMembers(this.getMembersProtobuf());
        builder.putAllParentCellCoordinators(this.getParentCellCoordinatorsProtobuf());
        builder.putAllDescendantCells(this.getDescendantCellsProtobuf());

        return builder.build();
    }

    public ByteString getByteString() {
        return this.getProtobuf().toByteString();
    }

    public void updateCurrentCell() {
        for (Map.Entry<String, P3Node> entry : this.getMembers().entrySet()) {
            networkManager.send(entry.getValue().getAddress(), entry.getValue().getPort(), "UPDATE", this.getByteString());
        }
    }

    public void updateParentCoordinators() {
        if (!parentCellCoordinators.isEmpty()) {
            for (Map.Entry<String, P3Node> entry : parentCellCoordinators.entrySet()) {
                networkManager.send(entry.getValue().getAddress(), entry.getValue().getPort(), "UPDATE CELL", this.getByteString());
            }
        }
    }

    public void updateParentCoordinatorsChildCells() {
        if (!descendantCells.isEmpty()) {
            for (Map.Entry<String, P3Cell> cellEntry : descendantCells.entrySet()) {
                for (Map.Entry<String, P3Node> nodes : cellEntry.getValue().getCoordinators().entrySet()) {
                    networkManager.send(nodes.getValue().getAddress(), nodes.getValue().getPort(), "UPDATE PARENT COORDINATORS", this.getByteString());
                }
            }
        }
    }

    public void electNewCoordinator() {
        for (P3Node node : this.members.values()) {
            if ( ! node.isCoordinator()) {
                this.coordinators.put(node.getId(), node);
                networkManager.send(node.getAddress(), node.getPort(), "ELECTED COORDINATOR", this.getByteString());
                break;
            }
        }

        this.updateParentCoordinators();
        this.updateParentCoordinatorsChildCells();
    }

    @Override
    public String toString() {
        return "[" + this.id + "] " + this.members;
    }
}
