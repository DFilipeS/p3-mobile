package com.dfilipes.p3mobilenetwork.network;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteOrder;

/**
 * Created by Daniel Filipe on 10/11/15.
 */
public class P3NetworkUtils {
    private static final String LOG_TAG = P3NetworkUtils.class.getSimpleName();

    public P3NetworkUtils() {
        // Empty constructor
    }

    /**
     * Fetches IP address of the Wifi connection on the device.
     * @param context Activity's context.
     * @return The IP address string or null if there is no Wifi connection.
     */
    public static String getIpAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        String ipAddressString;
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException ex) {
            Log.e(LOG_TAG, "Unable to get host address.");
            ipAddressString = null;
        }

        return ipAddressString;

    }
}