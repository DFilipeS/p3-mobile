package com.dfilipes.p3mobilenetwork.network;

import android.util.Log;

import com.dfilipes.p3mobilenetwork.network.topology.P3Cell;
import com.dfilipes.p3mobilenetwork.network.topology.P3Node;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Daniel Filipe on 23/02/16.
 */
public class P3CoordinationRoutines implements Runnable, Serializable {
    private final String LOG_TAG = P3CoordinationRoutines.class.getSimpleName();
    //private P3NetworkManager networkManager;
    private boolean stopThread = false;

    public P3CoordinationRoutines() {
        //this.networkManager = P3NetworkManager.getInstance();
    }

    public void stop() {
        this.stopThread = true;
    }

    @Override
    public void run() {
        P3NetworkManager networkManager = P3NetworkManager.getInstance();

        while (!stopThread) {
            Map<String, P3Node> cellMembers = networkManager.getCell().getMembers();
            for (Map.Entry<String, P3Node> entry : cellMembers.entrySet()) {
                networkManager.send(entry.getValue().getAddress(), entry.getValue().getPort(), "PING");
            }

            Map<String, P3Node> parentCellCoordinators = networkManager.getCell().getParentCellCoordinators();
            for (Map.Entry<String, P3Node> entry : parentCellCoordinators.entrySet()) {
                networkManager.send(entry.getValue().getAddress(), entry.getValue().getPort(), "PING");
            }

            Map<String, P3Cell> descendantCells = networkManager.getCell().getDescendantCells();
            for (Map.Entry<String, P3Cell> entryCell : descendantCells.entrySet()) {
                for (Map.Entry<String, P3Node> entryNode : entryCell.getValue().getCoordinators().entrySet()) {
                    networkManager.send(entryNode.getValue().getAddress(), entryNode.getValue().getPort(), "PING");
                }
            }

            try {
                Thread.currentThread().sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}