package com.dfilipes.p3mobilenetwork.network;

import com.dfilipes.p3mobilenetwork.NetworkGlobals;
import com.dfilipes.p3mobilenetwork.link.P3Sender;
import com.dfilipes.p3mobilenetwork.network.topology.P3Cell;
import com.dfilipes.p3mobilenetwork.network.topology.P3Node;
import com.google.protobuf.ByteString;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Daniel Filipe on 2015-11-17.
 */
public class P3JoinStateMachine {
    private P3NetworkManager networkManager;
    private P3Node node;

    private P3Cell availableCell = null;

    private int currentState = 0;
    private final int YES = 0;
    private final int NO = 1;
    private P3State[] states = {
            new State0(),   // "Is current cell full?"
            new State1(),   // "Are there any descendant cells?"
            new State2(),   // "Is there any descendant cell with available space?"
            new State3(),   // "The number of descendant cell is maximum allowed?"
            new State4(),   // "Join the current cell."
            new State5(),   // "Create new cell and become its coordinator."
            new State6(),   // "Redirect negotiation."
            new InvalidState(),
    };
    private int[][] transitions = {
            {1, 4},
            {2, 5},
            {6, 3},
            {6, 5},
            {7, 7},
            {7, 7},
            {7, 7},
            {7, 7},
    };

    public P3JoinStateMachine(P3Cell cell, P3Node node) {
        this.networkManager = P3NetworkManager.getInstance();
        this.node = node;

        states[currentState].run();
    }

    private void changeState(int actionCode) {
        currentState = transitions[currentState][actionCode];
        states[currentState].run();
    }

    abstract class P3State {
        public abstract void run();
    }

    /**
     * State 0 - INITIAL STATE
     * "Is current cell full?"
     */
    class State0 extends P3State {

        @Override
        public void run() {
            if (networkManager.getCell().isFull()) {
                P3JoinStateMachine.this.changeState(P3JoinStateMachine.this.YES);
            } else {
                P3JoinStateMachine.this.changeState(P3JoinStateMachine.this.NO);
            }
        }
    }

    /**
     * State 1
     * "Are there any descendant cells?"
     */
    class State1 extends P3State {

        @Override
        public void run() {
            if (networkManager.getCell().hasDescendantCells()) {
                P3JoinStateMachine.this.changeState(P3JoinStateMachine.this.YES);
            } else {
                P3JoinStateMachine.this.changeState(P3JoinStateMachine.this.NO);
            }
        }
    }

    /**
     * State 2
     * "Is there any descendant cell with available space?"
     */
    class State2 extends P3State {

        @Override
        public void run() {
            Map<String, P3Cell> descendantCells = networkManager.getCell().getDescendantCells();
            for (Map.Entry<String, P3Cell> entry : descendantCells.entrySet()) {
                if (!entry.getValue().isFull()) {
                    availableCell = entry.getValue();
                    break;
                }
            }

            if (availableCell == null) {
                P3JoinStateMachine.this.changeState(P3JoinStateMachine.this.NO);
            } else {
                P3JoinStateMachine.this.changeState(P3JoinStateMachine.this.YES);
            }
        }
    }

    /**
     * State 3
     * "The number of descendant cell is maximum allowed?"
     */
    class State3 extends P3State {

        @Override
        public void run() {
            if (networkManager.getCell().isDescendantsLevelFull()) {
                P3JoinStateMachine.this.changeState(P3JoinStateMachine.this.YES);
            } else {
                P3JoinStateMachine.this.changeState(P3JoinStateMachine.this.NO);
            }
        }
    }

    /**
     * State 4 - FINAL STATE
     * "Join the current cell."
     */
    class State4 extends P3State {

        @Override
        public void run() {
            // Join the current cell.
            String newNodeId = networkManager.getCell().getId() + ":" + networkManager.getCell().getNextId();
            node.setId(newNodeId);
            networkManager.getCell().addMember(node);

            // See if a new coordinator is needed to fill
            if (networkManager.getCell().getCoordinators().size() < NetworkGlobals.CELL_COORDINATORS) {
                networkManager.getCell().addCoordinator(node);
                networkManager.getCell().updateParentCoordinators();
            }

            try {
                InetAddress address = InetAddress.getByName(node.getAddress());
                new Thread(new P3Sender(address, node.getPort(), "JOIN OK", networkManager.getCell().getByteString(), ByteString.copyFrom(newNodeId.getBytes()))).run();
                networkManager.getCell().updateCurrentCell();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * State 5 - FINAL STATE
     * "Create new cell and become its coordinator."
     */
    class State5 extends P3State {

        @Override
        public void run() {
            String newCellId = networkManager.getCell().getId() + ":" + networkManager.getNextCellId();

            try {
                InetAddress address = InetAddress.getByName(node.getAddress());
                new Thread(new P3Sender(address, node.getPort(), "CREATE CELL", ByteString.copyFrom(newCellId.getBytes()), networkManager.getCell().getByteString())).run();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * State 6 - FINAL STATE
     * "Redirect negotiation."
     */
    class State6 extends P3State {

        @Override
        public void run() {
            P3Node coordinator = null;
            if (availableCell == null) {
                // Redirect negotiation to random cell.
                Map<String, P3Cell> descendants = networkManager.getCell().getDescendantCells();
                int randomId = ThreadLocalRandom.current().nextInt(0, descendants.size());
                P3Cell randomCell = descendants.get(networkManager.getCell().getId() + ":" + randomId);
                coordinator = randomCell.getCoordinators().entrySet().iterator().next().getValue();
            } else {
                // Redirect negotiation to the available cell.
                coordinator = availableCell.getCoordinators().entrySet().iterator().next().getValue();
            }

            try {
                InetAddress address = InetAddress.getByName(node.getAddress());
                new Thread(new P3Sender(address, node.getPort(), "JOIN REDIRECT", coordinator.getByteString())).run();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Invalid state
     */
    class InvalidState extends P3State {

        @Override
        public void run() {
            P3JoinStateMachine.this.networkManager.log("[P3JoinStateMachine] Invalid state. This shouldn't happen...");
        }
    }
}
