package com.dfilipes.p3mobilenetwork.network.topology;

import com.dfilipes.p3mobilenetwork.link.P3NetworkProtos;
import com.dfilipes.p3mobilenetwork.network.P3CoordinationRoutines;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.google.protobuf.ByteString;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Daniel Filipe on 11/11/15.
 */
public class P3Node implements Serializable {
    private static final long serialVersionUID = 42L;
    private String id;
    private String address;
    private int port;
    private int filesPort;

    private P3NetworkProtos.Node node;

    private P3CoordinationRoutines coordinationRoutines = null;

    public P3Node() {
        this.id = null;
        this.address = null;
        this.port = 0;
        this.filesPort = 0;
    }

    public P3Node(P3NetworkProtos.Node node) {
        this.id = node.getId();
        this.address = node.getAddress();
        this.port = node.getPort();
        this.filesPort = node.getFilesPort();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getFilesPort() {
        return filesPort;
    }

    public void setFilesPort(int filesPort) {
        this.filesPort = filesPort;
    }

    public P3NetworkProtos.Node getProtobuf() {
        P3NetworkProtos.Node.Builder builder = P3NetworkProtos.Node.newBuilder();
        builder.setId(this.id);
        builder.setAddress(this.address);
        builder.setPort(this.port);
        builder.setFilesPort(this.filesPort);

        return builder.build();
    }

    public ByteString getByteString() {
        return this.getProtobuf().toByteString();
    }

    public boolean isRoot() {
        if (this.id != null && this.id.equals("0:0")) {
            return true;
        }

        return false;
    }

    public boolean isCoordinator() {
        Map<String, P3Node> coordinators = P3NetworkManager.getInstance().getCell().getCoordinators();
        return coordinators.containsKey(this.id);
    }

    public void startCoordinationRoutines() {
        this.coordinationRoutines = new P3CoordinationRoutines();
        new Thread(this.coordinationRoutines).start();
    }

    public P3CoordinationRoutines getCoordinationRoutines() {
        return this.coordinationRoutines;
    }

    public void stopCoordinationRoutines() {
        this.coordinationRoutines.stop();
    }

    @Override
    public String toString() {
        return "[" + this.id + "] " + this.address + ":" + this.port;
    }
}
