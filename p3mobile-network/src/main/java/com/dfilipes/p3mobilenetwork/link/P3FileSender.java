package com.dfilipes.p3mobilenetwork.link;

import com.dfilipes.p3mobilenetwork.P3App;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Daniel Filipe on 2015-11-14.
 */
public class P3FileSender implements Runnable {
    private final String LOG_TAG = P3FileSender.class.getSimpleName();
    private P3NetworkManager networkManager;
    private InetAddress address;
    private int port;
    private String filename;

    public P3FileSender(InetAddress address, int port, String filename) {
        this.networkManager = P3NetworkManager.getInstance();
        this.address = address;
        this.port = port;
        this.filename = filename;

        networkManager.log("[SENT FILE] " + filename);
    }

    @Override
    public void run() {
        try {
            Socket socket = new Socket(address, port);

            File file = new File(P3App.getContext().getFilesDir() + "/" + filename);
            byte[] mybytearray = new byte[(int) file.length()];
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fis);
            bis.read(mybytearray, 0, mybytearray.length);

            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeUTF(filename);
            dos.writeLong(mybytearray.length);
            dos.write(mybytearray, 0, mybytearray.length);
            dos.flush();

            socket.close();
        } catch (IOException e) {
            // Node is dead. RIP :(
            if (networkManager.getMyDevice().isCoordinator()) {
                // Find node in the current cell or any descendant cell and remove it
                networkManager.log(address.getHostAddress() + ":" + port + " is dead");
                networkManager.removeDevice(address.getHostAddress(), port);
            }

            e.printStackTrace();
        }
    }
}