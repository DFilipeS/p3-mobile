package com.dfilipes.p3mobilenetwork.link;

import com.dfilipes.p3mobilenetwork.network.topology.P3Node;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * Created by Daniel Filipe on 2015-11-14.
 */
public class P3Message implements Serializable {
    private static final long serialVersionUID = 42L;

    public String title;
    public P3Node sender;
    public LinkedHashSet<Object> payload;

    public P3Message(String title, P3Node sender) {
        this.title = title;
        this.sender = sender;
        payload = new LinkedHashSet<>();
    }

    public P3Message (String title, P3Node sender, Object... objects) {
        this.title = title;
        this.sender = sender;
        payload = new LinkedHashSet<>();
        payload.addAll(Arrays.asList(objects));
    }

    public void addToPayload(Object object) {
        payload.add(object);
    }

    public void addToPayload(Collection collection) {
        payload.addAll(collection);
    }
}
