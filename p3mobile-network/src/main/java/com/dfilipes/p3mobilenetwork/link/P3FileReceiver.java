package com.dfilipes.p3mobilenetwork.link;

import android.content.Context;

import com.dfilipes.p3mobilenetwork.P3App;
import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Daniel Filipe on 2015-11-14.
 */
public class P3FileReceiver implements Runnable {
    private P3NetworkManager networkManager;
    private int port;
    private ServerSocket serverSocket;

    public P3FileReceiver() {
        this.networkManager = P3NetworkManager.getInstance();
        this.port = 0;
    }

    @Override
    public void run() {
        Socket socket = null;

        try {
            serverSocket = new ServerSocket(port);

            if (this.port == 0) {
                networkManager.getMyDevice().setFilesPort(serverSocket.getLocalPort());
            }

            networkManager.log("Listening for incoming connections on " + InetAddress.getLocalHost().getHostAddress() + ":" + serverSocket.getLocalPort());

            while (!Thread.currentThread().isInterrupted()) {
                socket = serverSocket.accept();

                DataInputStream in = new DataInputStream(socket.getInputStream());
                String fileName = in.readUTF();
                long size = in.readLong();

                // Read from the socket.
                FileOutputStream output = P3App.getContext().openFileOutput(fileName, Context.MODE_PRIVATE);
                byte[] buffer = new byte[((int) size)];
                in.readFully(buffer, 0, buffer.length);
                output.write(buffer, 0, buffer.length);

                output.close();
                in.close();
                socket.close();

                // TODO : Notify someone about the file
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
