package com.dfilipes.p3mobilenetwork.link;

import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;
import com.dfilipes.p3mobilenetwork.network.topology.P3Node;
import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Daniel Filipe on 2015-11-14.
 */
public class P3Sender implements Runnable {
    private final String LOG_TAG = P3Sender.class.getSimpleName();
    private P3NetworkManager networkManager;
    private InetAddress address;
    private int port;
    private P3NetworkProtos.Message message;

    public P3Sender(InetAddress address, int port, String title, ByteString... objects) {
        this.networkManager = P3NetworkManager.getInstance();
        this.address = address;
        this.port = port;

        // Protobuf Node object build
        P3Node myDevice = networkManager.getMyDevice();
        P3NetworkProtos.Node.Builder senderBuilder = P3NetworkProtos.Node.newBuilder();
        if (myDevice.getId() != null) {
            senderBuilder.setId(myDevice.getId());
        }
        senderBuilder.setAddress(myDevice.getAddress());
        senderBuilder.setPort(myDevice.getPort());
        senderBuilder.setFilesPort(myDevice.getFilesPort());

        // Protobuf Message object build
        P3NetworkProtos.Message.Builder messageBuilder = P3NetworkProtos.Message.newBuilder();
        messageBuilder.setTitle(title);
        messageBuilder.setSender(senderBuilder.build());
        for (ByteString object : objects) {
            messageBuilder.addPayload(object);
        }

        this.message = messageBuilder.build();

        networkManager.log("[SENT] " + title);

        /** Message size testing **/
        P3Message testMessage = new P3Message(title, networkManager.getMyDevice());
        testMessage.addToPayload(objects);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(outputStream);
            out.writeObject(testMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }

        networkManager.protoBufByteCounter += this.message.toByteArray().length;
        networkManager.serializationByteCounter += outputStream.toByteArray().length;
        networkManager.log("STATS", networkManager.protoBufByteCounter + " bytes with protocol buffers");
        networkManager.log("STATS", networkManager.serializationByteCounter + " bytes with serialization");
    }

    @Override
    public void run() {
        try {
            Socket socket = new Socket(address, port);

            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            message.writeTo(objectOutputStream);

            objectOutputStream.close();
            socket.close();
        } catch (IOException e) {
            // Node is dead. RIP :(
            if (networkManager.getMyDevice().isCoordinator()) {
                // Find node in the current cell or any descendant cell and remove it
                networkManager.log(address.getHostAddress() + ":" + port + " is dead");
                networkManager.removeDevice(address.getHostAddress(), port);
            }

            e.printStackTrace();
        }
    }
}
