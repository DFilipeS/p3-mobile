package com.dfilipes.p3mobilenetwork.link;

import com.dfilipes.p3mobilenetwork.network.P3NetworkManager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Daniel Filipe on 2015-11-14.
 */
public class P3Receiver implements Runnable {
    private P3NetworkManager networkManager;
    private int port;
    private ServerSocket serverSocket;

    public P3Receiver() {
        this.networkManager = P3NetworkManager.getInstance();
        this.port = 0;
    }

    public P3Receiver(int port) {
        this.networkManager = P3NetworkManager.getInstance();
        this.port = port;
    }

    @Override
    public void run() {
        Socket socket = null;

        try {
            serverSocket = new ServerSocket(port);

            if (this.port == 0) {
                networkManager.getMyDevice().setPort(serverSocket.getLocalPort());
            }

            networkManager.log("Listening for incoming connections on " + InetAddress.getLocalHost().getHostAddress() + ":" + serverSocket.getLocalPort() + " (if this address is not correct, check your network interfaces).");

            while (!Thread.currentThread().isInterrupted()) {
                socket = serverSocket.accept();
                //networkManager.log("New connection arrived from " + socket.getInetAddress() + ":" + socket.getPort() + ".");

                // Read from the socket.
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                //P3Message message = (P3Message) objectInputStream.readObject();
                P3NetworkProtos.Message message = P3NetworkProtos.Message.parseFrom(objectInputStream);
                networkManager.processResponse(message);

                objectInputStream.close();
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
