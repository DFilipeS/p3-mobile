package com.dfilipes.p3mobilenetwork;

/**
 * Created by Daniel Filipe on 11/11/15.
 */
public class NetworkGlobals {
    public static String SERVER_IP = "192.168.1.71";
    public static int SERVER_PORT = 4242;
    public static int CELL_SIZE = 4;
    public static int CELL_COORDINATORS = 2;
    public static int MAX_CELLS_PER_LEVEL = 2;

    public static String INTERFACE_IP = "192.168.1.71";
}
